## mujadara

A tool that uses GitLab's CI [config validation API endpoint](https://docs.gitlab.com/ce/api/lint.html) to validate local config files.

### Usage

> :warning: Since GitLab 13.7.7 (2021-02-11) authentication is required, so you will need to use `--token=$ACCESS_TOKEN` or `--host=http://$USERNAME:$PASSWORD@gitlab.com`

One or more `.gitlab-ci.yml` are passed as arguments on the command line. Any errors will result in a non-zero exit code. The filename must end in `.yml` to pass, but doesn't have to be `.gitlab-ci.yml`.

An access token must be provided in order to authenticate with the gitlab API. You can see your access tokens through [your profile settings](https://gitlab.com/-/profile/personal_access_tokens). The token must have at least the "api" scope.

```text
$ gitlab-ci-validate --token=ACCESS_TOKEN ./good.yml ./maybe-good.yml ./bad.yml
PASS: ./good.yml
SOFT FAIL: ./maybe-good.yml
 - Post https://gitlab.com/api/v4/ci/lint: dial tcp: lookup gitlab.com on 127.0.0.53:53: read udp 127.0.0.1:41487->127.0.0.53:53: i/o timeout
HARD FAIL: ./bad.yml
 - jobs:storage config contains unknown keys: files
```

Each input file will be validated and one of 3 results will be printed for it:

- _PASS_ - the file passed all checks
- _SOFT FAIL_ - the file is acessable and contains valid YAML, but there was an error contacting the validation API
- _HARD FAIL_ - the file failed any checks

The exit code will be:

- 0 if all files are valid (all _PASS_)
- 1 if any files are invalid (any _HARD FAIL_)
- 2 if there was any _SOFT FAIL_ and no _HARD FAIL_
